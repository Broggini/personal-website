$('div.aboutme_menu').click(function(){
    $('div.aboutme').css('display', 'block');
    $('div.aboutme_menu').css('background-color', '#00496f');
    $('span.current_page').text('about_me.py');

    var myArray = ["projects", "publications"];
    $.each(myArray, function (index, value) {
        $('div.'+value).css('display', 'none');
        $('div.'+value+'_menu').css('background-color', '#2A2A2A');
    });
})

$('div.projects_menu').click(function(){

    $('div.projects').css('display', 'block');
    $('div.projects_menu').css('background-color', '#00496f');
    $('span.current_page').text('projects.json');

    var myArray = ["aboutme", "publications"];
    $.each(myArray, function (index, value) {
        $('div.'+value).css('display', 'none');
        $('div.'+value+'_menu').css('background-color', '#2A2A2A');
    });
})


$('div.publications_menu').click(function(){

    $('div.publications').css('display', 'block');
    $('div.publications_menu').css('background-color', '#00496f');
    $('span.current_page').text('publications.xml');

    var myArray = ["aboutme", "projects" ];
    $.each(myArray, function (index, value) {
        $('div.'+value).css('display', 'none');
        $('div.'+value+'_menu').css('background-color', '#2A2A2A');
    });
})

